This is a basic SAT solver developed by
F. Maurica (fonenantsoa.maurica@gmail.com), http://lim.univ-reunion.fr/staff/fmaurica/

Current implemented techniques:
 + https://www.lri.fr/~filliatr/ftp/tp-info/IFexpressions.ps.gz



INSTALLATION
=============
On Ubuntu 20.04:
1) Install the ocaml package, mine's version is 4.08.1
2) Clone this repository
3) At the base directory of the repository, launch:
   $ make

Now you have the executable file named sat



USAGE
=============
$./sat <prop>

where:
<prop> ::=  T
          | F
          | <atom>
          | ~ <prop>
          | <prop> & <prop>
          | <prop> | <prop>
          | <prop> -> <prop>
          | <prop> <-> <prop>
          | (<prop>)
<atom> ::= ^[a-z]+[0-9]*$

If <prop> is SAT
  then ./sat prints a _partial_ assignment for the atoms (the variables) that renders <prop> to be True
  else ./sat prints (,false)



EXAMPLES
=============
$ ./sat '(a&b | c)->d  <->  ~e'
(a,true) (b,true) (d,true) (e,false)

$ ./sat 'p & F'
(,false)
